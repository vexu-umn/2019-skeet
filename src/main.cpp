#include "main.h"
#include "systems.h"
#include "gopher/utils.h"
#include "gopher/drivetrain.h"
#include "gopher/GopherMotor.h"
#include "gopher/current_logger.h"

using namespace okapi::literals;

// wheelbase * pi * 10 = 180.2437 ? Revalidate numbers
std::shared_ptr<TankDrive> drive;
std::shared_ptr<Intake> intake;
std::shared_ptr<DescoreArm> descore;
std::shared_ptr<PushyArm> pushy;
std::shared_ptr<Wedge> wedge;
std::shared_ptr<Puncher> puncher;

std::vector<std::pair<std::string, auton_ptr>> autons;
std::shared_ptr<ScreenInterface> interface;
std::shared_ptr<pros::Task> currentLogTask;

std::shared_ptr<okapi::MotorGroup> leftMotors;
std::shared_ptr<okapi::MotorGroup> rightMotors;

void ryan_auto();
void block_auto();
void ryan_main();

void initialize() {
	std::cout << "Init" << std::endl;
	// Init systems
	leftMotors = std::make_shared<okapi::MotorGroup>(
        std::initializer_list<okapi::Motor>({okapi::Motor(6,  true, DRIVE_GEARSET),
                                             okapi::Motor(7,  true, DRIVE_GEARSET), 
                                             okapi::Motor(8,  true, DRIVE_GEARSET),
											 okapi::Motor(9,  true, DRIVE_GEARSET),
											 okapi::Motor(10, true, DRIVE_GEARSET)}));
    rightMotors = std::make_shared<okapi::MotorGroup>(
        std::initializer_list<okapi::Motor>({okapi::Motor(1, false, DRIVE_GEARSET),
                                             okapi::Motor(2, false, DRIVE_GEARSET), 
                                             okapi::Motor(3, false, DRIVE_GEARSET),
											 okapi::Motor(4, false, DRIVE_GEARSET),
											 okapi::Motor(5, false, DRIVE_GEARSET)}));
	registerMotor(6, leftMotors);
	registerMotor(1, rightMotors);				 
	leftMotors->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
	rightMotors->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
	leftMotors->setReversed(true);
	rightMotors->setReversed(false);

	std::shared_ptr<GopherMotor> wedgem = std::make_shared<GopherMotor>(-11);
	std::shared_ptr<GopherMotor> intakem = std::make_shared<GopherMotor>(20);
	std::shared_ptr<GopherMotor> feederm = std::make_shared<GopherMotor>(13);
	std::shared_ptr<GopherMotor> descorem = std::make_shared<GopherMotor>(12);
	std::shared_ptr<GopherMotor> pushyM = std::make_shared<GopherMotor>(19);
	std::shared_ptr<GopherMotor> punchArticM = std::make_shared<GopherMotor>(18);
	std::shared_ptr<okapi::AbstractMotor> punchMotors = std::make_shared<okapi::MotorGroup>(std::initializer_list<okapi::Motor>({okapi::Motor(16),okapi::Motor(-17)}));
	registerMotor(16, punchMotors);

	wedgem->registerToCurrentLogger();
	intakem->registerToCurrentLogger();
	feederm->registerToCurrentLogger();
	descorem->registerToCurrentLogger();
	pushyM->registerToCurrentLogger();
	punchArticM->registerToCurrentLogger();

	drive = std::make_shared<TankDrive>(leftMotors, rightMotors, 4_in, 13.5_in);
	wedge = std::make_shared<Wedge>(wedgem);
	intake = std::make_shared<Intake>(intakem, feederm, wedge);
	descore = std::make_shared<DescoreArm>(descorem);
	pushy = std::make_shared<PushyArm>(pushyM);
	puncher = std::make_shared<Puncher>(punchArticM, punchMotors);

	std::cout << "Done creating systems" << std::endl;
	interface = std::make_shared<ScreenInterface>(&autons, drive);

	std::cout << "Generating paths..." << std::endl;
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {40_in, 0_in, 0_deg}}, "Block");
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {50_in, 0_in, 0_deg}}, "Flip_Bottom");
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {40_in, 0_in, 0_deg}}, "Cap_Ball");
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {42_in, 45_in, 90_deg}}, "Cap_Ball1_red");
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {42_in, -45_in, -90_deg}}, "Cap_Ball1_blue");
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {18_in, -24_in, 90_deg}}, "Park_red");
	drive->genPathAndStore({{0_in, 0_in, 0_deg}, {18_in, 24_in, -90_deg}}, "Park_blue");

	std::cout<< "Ready!" << std::endl;
	autons.push_back(std::pair<std::string, auton_ptr>("Low Flag/Shoot/Park", &ryan_auto));
	autons.push_back(std::pair<std::string, auton_ptr>("Block", &block_auto));

	interface->initialize();
	//currentLogTask = startLoggerTask();

}

void disabled() {}

void competition_initialize() {}

void ryan_auto() {
	puncher->aimLow();
	//FLIP BOTTOM FLAG
	drive->loadPathAndRunReverse("Flip_Bottom");
	drive->waitUntilSettled();
	ryan_main();
}

void ryan_main() {
	bool isRed = interface->getColor() == AllianceColor::red;
	

	std::string capb1str = "Cap_Ball1_blue";
	if (isRed) {
		capb1str = "Cap_Ball1_red";
	}
	//GET SECOND BALL
	intake->intake();
	drive->loadPathAndRun(capb1str);
	drive->waitUntilSettled();
	drive->removePath(capb1str);
	drive->loadPathAndRunReverse("Cap_Ball");
	pros::delay(1000);
	wedge->setDown();
	pros::delay(200);
	intake->idle();
	drive->waitUntilSettled();
	drive->removePath("Cap_Ball");
	
	float dir = 1;
	if (isRed) {
		dir = -1;
	}
	drive->getChassis()->turnAngle(dir * 90_deg * 2.3);

	puncher->doPunch();
	pros::delay(500);
	intake->intake();
	pros::delay(1200);
	puncher->aimHigh();
	puncher->doPunch();
	pros::delay(1000);
	wedge->setUp();
	intake->idle();

	std::string parkstr = "Park_blue";
	if (isRed) {
		parkstr = "Park_red";
	}

	drive->loadPathAndRunReverse(parkstr);
	drive->waitUntilSettled();
	drive->removePath(parkstr);
	pushy->setOut();
	drive->getChassis()->moveDistance(-16_in);
}

void block_auto() {
	bool isRed = interface->getColor() == AllianceColor::red;
	puncher->aimLow();

	// Turn 90
	float dir = -1;
	if (isRed) {
		dir = 1;
	}
	
	drive->getChassis()->turnAngle(dir * 90_deg * 2.3);
	// Drive back
	drive->loadPathAndRunReverse("Block");
	drive->waitUntilSettled();
	pushy->setOut();
	pros::delay(10 * 1000); // Wait 10s
	drive->drivePath("Block");
	pros::delay(1000);
	pushy->setIn();
	drive->waitUntilSettled();
	drive->getChassis()->turnAngle(-dir * 90_deg * 2.3); // Get back into position

	ryan_main();
}

/**
 * Runs the user autonomous code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the autonomous
 * mode. Alternatively, this function may be called in initialize or opcontrol
 * for non-competition testing purposes.
 *
 * If the robot is disabled or communications is lost, the autonomous task
 * will be stopped. Re-enabling the robot will restart the task, not re-start it
 * from where it left off.
 */
void autonomous() {
	auton_ptr v = interface->getSelectedAuton();
	(*v)();
}

void opcontrol() {
	std::cout << "Op control starting" << std::endl;
	rightMotors->setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
	leftMotors->setBrakeMode(okapi::AbstractMotor::brakeMode::coast);
	pros::Controller master(pros::E_CONTROLLER_MASTER);
	while (true) {
		double power = master.get_analog(ANALOG_LEFT_Y) / 127.0;
		double turn = master.get_analog(ANALOG_RIGHT_X)  / 127.0;

		drive->getChassis()->arcade(power, turn);

		if (descore->getState() == DescoreArm::STATE_IDLE) {
			if (master.get_digital(DIGITAL_RIGHT)) {
				descore->setSpeed(-1.0);
			}
			else if (master.get_digital(DIGITAL_LEFT)) {
				descore->setSpeed(1.0);
			}
			else {
				descore->setSpeed(0);
			}
		}
		if (master.get_digital_new_press(DIGITAL_A)) {
			descore->descoreCap();
		}
		if (master.get_digital_new_press(DIGITAL_B)) {
			descore->readyDescore();
		}
		if (master.get_digital_new_press(DIGITAL_X)) {
			descore->reset();
		}

		static bool wedgeOut = false;
		static bool pushyOut = false;

		if (master.get_digital_new_press(DIGITAL_R2)) {
			if (pushyOut) {
				pushy->setIn();
			}
			else {
				pushy->setOut();
				wedge->setUp();
				wedgeOut = false;
			}
			pushyOut = !pushyOut;
		}

		if (master.get_digital_new_press(DIGITAL_R1)) {
			if (wedgeOut) {
				wedge->setUp();
				
			}
			else {
				wedge->setDown();
				pushy->setIn();
				pushyOut = false;
			}
			wedgeOut = !wedgeOut;
		}

		if (master.get_digital(DIGITAL_L1)) {
			intake->intake();
		}
		else {
			intake->idle();
		}

		if (master.get_digital_new_press(DIGITAL_L2)) {
			puncher->doPunch();
		}

		if (master.get_digital_new_press(DIGITAL_DOWN)) {
			puncher->aimLow();
		}
		else if (master.get_digital_new_press(DIGITAL_UP)) {
			puncher->aimHigh();
		}

		pros::delay(20);
	}
}