#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"

Wedge::Wedge(std::shared_ptr<okapi::AbstractMotor> articulate) {
    this->articulate = articulate;
    this->articulate->setGearing(okapi::AbstractMotor::gearset::red);
    this->articulate->setEncoderUnits(okapi::AbstractMotor::encoderUnits::degrees);
    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
    this->articulate->tarePosition();
}

void Wedge::setDown() {
    this->state = Wedge::STATE_MOVING_DOWN;
    this->articulate->moveAbsolute(180.0, getVelocityFromGearset(this->articulate->getGearing()));
}

void Wedge::setUp() {
    this->state = Wedge::STATE_MOVING_UP;
    this->articulate->moveAbsolute(10, 0.6*getVelocityFromGearset(this->articulate->getGearing()));
}

void Wedge::loop() {

    if (this->state == Wedge::STATE_MOVING_DOWN && std::abs(this->getPosError()) < Wedge::TARGET_ERR) {
        this->state = Wedge::STATE_DOWN;
    }
    if (this->state == Wedge::STATE_MOVING_UP && std::abs(this->getPosError()) < Wedge::TARGET_ERR) {
        this->state = Wedge::STATE_UP;
    }
};

float Wedge::getPosError() {
    return this->articulate->getTargetPosition() - this->articulate->getPosition();
}

Wedge::~Wedge() {

}

int Wedge::getState() {
    return this->state;
}
