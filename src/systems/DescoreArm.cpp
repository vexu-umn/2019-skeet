#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"

DescoreArm::DescoreArm(std::shared_ptr<okapi::AbstractMotor> articulate) {
    this->articulate = articulate;
    this->articulate->setGearing(okapi::AbstractMotor::gearset::red);
    this->articulate->setEncoderUnits(okapi::AbstractMotor::encoderUnits::degrees);
    this->articulate->tarePosition();
    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
}

void DescoreArm::setSpeed(float speed) {
    if (this->state == DescoreArm::STATE_IDLE) {
        this->articulate->moveVelocity(speed * getVelocityFromGearset(this->articulate->getGearing()));
    }
}

void DescoreArm::descoreCap() {
    this->state = DescoreArm::STATE_DESCORE_RISING;
    this->articulate->moveAbsolute(180 * DESCORE_GEAR_RATIO, getVelocityFromGearset(this->articulate->getGearing()));
}

void DescoreArm::readyDescore() {
    this->state = DescoreArm::STATE_READYING;
    this->articulate->moveAbsolute(90 * DESCORE_GEAR_RATIO, getVelocityFromGearset(this->articulate->getGearing()));
}

void DescoreArm::reset() {
    this->state = DescoreArm::STATE_ZEROING;
    this->articulate->moveAbsolute(0, getVelocityFromGearset(this->articulate->getGearing()));
}

DescoreArm::~DescoreArm() {
    
}

void DescoreArm::loop() {
    if (this->state == DescoreArm::STATE_DESCORE_RISING) {
        if (this->isAtTarget()) {
            this->state = DescoreArm::STATE_DESCORE_LOWERING;
            this->articulate->moveAbsolute(90, getVelocityFromGearset(this->articulate->getGearing()));
        }
    }
    if (this->state == DescoreArm::STATE_DESCORE_LOWERING) {
        if (this->isAtTarget()) {
            this->state = DescoreArm::STATE_IDLE;
        }
    }
    if (this->state == DescoreArm::STATE_READYING && this->isAtTarget()) {
        this->state = DescoreArm::STATE_IDLE;
    }
    if (this->state == DescoreArm::STATE_ZEROING && this->isAtTarget()) {
        this->state = DescoreArm::STATE_IDLE;
    }
};

bool DescoreArm::isAtTarget() {
    return std::abs(this->articulate->getTargetPosition() - this->articulate->getPosition()) < 5;
}

int DescoreArm::getState() {
    return this->state;
}