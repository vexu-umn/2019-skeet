#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"

PushyArm::PushyArm(std::shared_ptr<okapi::AbstractMotor> articulate) {
    this->articulate = articulate;
    this->articulate->setGearing(okapi::AbstractMotor::gearset::red);
    this->articulate->setEncoderUnits(okapi::AbstractMotor::encoderUnits::rotations);
    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
    this->articulate->tarePosition();
}

void PushyArm::setOut() {
    this->articulate->moveAbsolute(0.21 * PUSHYARM_GEAR_RATIO, getVelocityFromGearset(this->articulate->getGearing()));
}

void PushyArm::setIn() {
    this->articulate->moveAbsolute(0, getVelocityFromGearset(this->articulate->getGearing()));
}

PushyArm::~PushyArm() {
    
}

void PushyArm::loop() {

}