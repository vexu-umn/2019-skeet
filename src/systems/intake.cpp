#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"


void Intake::intake() {
    this->state = Intake::STATE_INTAKE;
}

void Intake::idle() {
    this->state = Intake::STATE_IDLE;
}

Intake::Intake(std::shared_ptr<okapi::AbstractMotor> intakem, std::shared_ptr<okapi::AbstractMotor> feederm, std::shared_ptr<Wedge> theWedge) {
    this->intakem = intakem;
    this->feederm = feederm;
    this->feederm->setGearing(okapi::AbstractMotor::gearset::blue);
    this->intakem->setGearing(okapi::AbstractMotor::gearset::blue);
    this->wedge = theWedge;
}

Intake::~Intake() {

}

int Intake::getState() {
    return this->state;
}

void Intake::loop() {
    bool overrideIntakeM = false;
    if (!wedge->getState() == Wedge::STATE_UP) {
        overrideIntakeM = true;
        if (wedge->getState() == Wedge::STATE_MOVING_DOWN) {
            this->intakem->moveVelocity(600);
        }
        else if (wedge->getState() == Wedge::STATE_MOVING_UP) {
            this->intakem->moveVelocity(-600);
        }
        else {
            this->intakem->moveVelocity(0);
        }
    }
    switch(this->state) {

        case Intake::STATE_INTAKE:
        this->feederm->moveVelocity(getVelocityFromGearset(this->feederm->getGearing()));
        if (!overrideIntakeM) this->intakem->moveVelocity(getVelocityFromGearset(this->intakem->getGearing()));
        break;

        default:
        case Intake::STATE_IDLE:
        this->feederm->moveVelocity(0);
        if (!overrideIntakeM) this->intakem->moveVelocity(0);
    }
}