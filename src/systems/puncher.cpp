#include "systems.h"
#include "okapi/api.hpp"
#include "gopher/utils.h"

Puncher::Puncher(std::shared_ptr<okapi::AbstractMotor> articulate, std::shared_ptr<okapi::AbstractMotor> punch) {
    this->articulate = articulate;
    this->punch = punch;
    this->punch->setBrakeMode(okapi::AbstractMotor::brakeMode::brake);
    this->punch->setGearing(okapi::AbstractMotor::gearset::red);
    this->punch->setEncoderUnits(okapi::AbstractMotor::encoderUnits::rotations);

    this->articulate->setBrakeMode(okapi::AbstractMotor::brakeMode::hold);
    this->articulate->setGearing(okapi::AbstractMotor::gearset::red);
    this->articulate->setEncoderUnits(okapi::AbstractMotor::encoderUnits::degrees);
    this->articulate->tarePosition();
};

Puncher::~Puncher() {

}

void Puncher::doPunch() {
    this->punch->moveRelative(1, getVelocityFromGearset(this->punch->getGearing()));
}

void Puncher::aimHigh() {
    this->articulate->moveAbsolute(PUNCHER_ARTIC_GEAR_RATIO * 34, getVelocityFromGearset(this->articulate->getGearing()));
}
void Puncher::aimLow() {
    this->articulate->moveAbsolute(PUNCHER_ARTIC_GEAR_RATIO * 50, getVelocityFromGearset(this->articulate->getGearing()));
}

void Puncher::loop() {

}