
#pragma once

#include "gopher/ramsete_controller.h"
#include <queue>
#include "gopher/pose_estimator.h"
#include "okapi/api.hpp"
#include "gopher/system.h"

using namespace okapi::literals;

// Theoretical top speed is ~1 m/s
#define DRIVE_MP_SPEED 0.5 // m/s
#define DRIVE_MP_ACC 0.5 // m/s^2
#define DRIVE_MP_JERK 10 // m/s^3
#define DRIVE_WHEEL_DIA 4_in
#define DRIVE_WHEELBASE 5.37_in
#define DRIVE_GEARSET okapi::AbstractMotor::gearset::green
#define DRIVE_TURN_P 0.6  // %vbus / rad error
#define DRIVE_TURN_I 0.0
#define DRIVE_TURN_D 0.0

#define PUNCHER_ARTIC_GEAR_RATIO 60/12

class Puncher : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;
    std::shared_ptr<okapi::AbstractMotor> punch;

    void loop() override;

    public:
    void doPunch();
    void aimHigh();
    void aimLow();

    Puncher(std::shared_ptr<okapi::AbstractMotor> articulate, std::shared_ptr<okapi::AbstractMotor> punch);
    ~Puncher();

    static const int STATE_IDLE = 0;
    static const int STATE_PUNCHING = 1;
};

#define PUSHYARM_GEAR_RATIO 84/12

class PushyArm : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;

    void loop() override;

    public:
    PushyArm(std::shared_ptr<okapi::AbstractMotor> articulate);
    ~PushyArm();

    void setIn();
    void setOut();
};

#define DESCORE_GEAR_RATIO 60/36

class DescoreArm : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;

    void loop() override;
    int state = DescoreArm::STATE_IDLE;

    bool isAtTarget();

    public:
    DescoreArm(std::shared_ptr<okapi::AbstractMotor> articulate);
    ~DescoreArm();

    void setSpeed(float speed);

    void descoreCap();
    int getState();
    void readyDescore();
    void reset();
    static const int STATE_IDLE = 0;
    static const int STATE_DESCORE_RISING = 1;
    static const int STATE_DESCORE_LOWERING = 2;
    static const int STATE_READYING = 3;
    static const int STATE_ZEROING = 4;
};

class Wedge : public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> articulate;
    int state = Wedge::STATE_UP;

    void loop() override;

    protected:
    float getPosError();

    public:
    Wedge(std::shared_ptr<okapi::AbstractMotor> articulate);
    ~Wedge();

    void setDown();
    void setUp();
    int getState();

    static constexpr float TARGET_ERR = 10.0; // degrees

    static const int STATE_UP = 0;
    static const int STATE_DOWN = 1;
    static const int STATE_MOVING_UP = 2;
    static const int STATE_MOVING_DOWN = 3;
};

class Intake: public System {
    private:
    std::shared_ptr<okapi::AbstractMotor> intakem;
    std::shared_ptr<okapi::AbstractMotor> feederm;
    int state = Intake::STATE_IDLE;
    std::shared_ptr<Wedge> wedge;

    void loop() override;

    public:
    void intake();
    void idle();
    int getState();

    Intake(std::shared_ptr<okapi::AbstractMotor> intake, std::shared_ptr<okapi::AbstractMotor> feederm, std::shared_ptr<Wedge> theWedge);
    ~Intake();

    static const int STATE_IDLE = 0;
    static const int STATE_INTAKE = 1;
};